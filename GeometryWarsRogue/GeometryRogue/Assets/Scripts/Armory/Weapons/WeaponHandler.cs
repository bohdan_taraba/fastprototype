﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class WeaponHandler : MonoBehaviour{


    public WeaponType type;
    public GameObject weaponObject;
    public float shotMovementSpeed;
    public float shotDelay;

    public float gunDamage;

    float shotDelayTimer;

    void Start () {
        shotDelayTimer = 0;
	}
	
	protected virtual void Update () {
        shotDelayTimer -= Time.deltaTime;
	}

    /// <summary>
    /// used for shots with specific direction
    /// </summary>
    /// <param name="shotDirection">direction of shot to pass</param>
    public virtual void Shot(Vector2 shotDirection)
    {
        GameObject o = Instantiate(weaponObject,transform.position,Quaternion.identity);
        o.GetComponent<ShotBase>().shotDirection = shotDirection;
    }

    /// <summary>
    /// used for shots without any specific parameters
    /// </summary>
    public virtual void Shot()
    {
        if (shotDelayTimer <= 0)
        {
            GameObject o = Instantiate(weaponObject, transform.position, transform.rotation);
            o.GetComponent<ShotBase>().shotSpeed = shotMovementSpeed;
            o.GetComponent<ShotBase>().shotDamage = gunDamage;
            shotDelayTimer = shotDelay;
        }
    }

    public void activateWeapon()
    {
        enabled = true;
    }

    public void deactivateWeapon()
    {
        enabled = false;
    }
}
