﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    Simple = 0,
    DoubleShot = 1,
    Laser = 2
}