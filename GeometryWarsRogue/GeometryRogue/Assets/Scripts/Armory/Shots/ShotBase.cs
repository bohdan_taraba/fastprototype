﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotBase : MonoBehaviour {

    [HideInInspector]
    public float shotSpeed;

    [HideInInspector]
    public Vector3 shotDirection;

    [HideInInspector]
    public float shotDamage;


	void Start () {
	}
	
	// Update is called once per frame
	public virtual void Update () {

    }

    public void destroyShot()
    {
        Destroy(gameObject);
    }

    public void playSound()
    {

    }
}
