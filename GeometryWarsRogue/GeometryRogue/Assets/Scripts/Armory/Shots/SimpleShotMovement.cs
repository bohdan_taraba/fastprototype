﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleShotMovement : ShotBase {


	void Start () {
        shotDirection = PointerHandler.positionOfPointerToPlayer();
	}
	
	public override void Update () {
        transform.position += shotDirection * shotSpeed * Time.deltaTime;
        base.Update();
    }
}
