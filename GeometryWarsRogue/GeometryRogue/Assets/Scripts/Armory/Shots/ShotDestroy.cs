﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShotDestroy : MonoBehaviour {

    public bool isChildren;
    SpriteRenderer sr;

    void Start () {
        sr = GetComponent<SpriteRenderer>();	
	}
	
	void Update () {
        checkDestroy();
	}

    void checkDestroy()
    {
        if(isChildren && !sr.isVisible)
        {
            Destroy(transform.parent.gameObject);
        }else if(!sr.isVisible)
        {
            Destroy(gameObject);
        }

    }
}
