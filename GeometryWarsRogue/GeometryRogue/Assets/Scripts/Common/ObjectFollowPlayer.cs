﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFollowPlayer : MonoBehaviour {

    public float followSpeed;

    Transform player;

	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
	}
	
	void Update () {
        followPlayer();		
	}

    void followPlayer()
    {
        if(player != null)
            transform.position = Vector3.MoveTowards(transform.position,player.position,followSpeed * Time.deltaTime);
    }
}
