﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameLivesCounter : MonoBehaviour {

    PlayerLife playerLife;
    TextMesh textMesh;

	void Start () {
        playerLife = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerLife>();
        textMesh = GetComponent<TextMesh>();
    }
	
	void Update () {
        textMesh.text = getSymbols();
	}

    string getSymbols()
    {
        string t = "";
        for (int i = 0; i < playerLife.numberOfLives; i++)
        {
            t += "♥ ";
        }
        return t;
    }
}
