﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngameLevelCounter : MonoBehaviour {


    TextMesh textMesh;

    void Start()
    {
        textMesh = GetComponent<TextMesh>();
    }

    void Update()
    {
        textMesh.text = "Level " + LevelingHandler.actualLevel;
    }
}
