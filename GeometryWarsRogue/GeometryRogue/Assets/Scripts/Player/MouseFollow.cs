﻿using UnityEngine;
using System.Collections;

public class MouseFollow : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}

    void Update()
    {
        //if (Input.GetMouseButton(0))
        RotateToMouse();
    }

    void RotateToMouse()
    {
        Vector3 diff = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        diff.Normalize();

        float rot_z = Mathf.Atan2(diff.y, diff.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rot_z - 90);
    }
}
