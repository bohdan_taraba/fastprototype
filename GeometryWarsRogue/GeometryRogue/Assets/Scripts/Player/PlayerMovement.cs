﻿using UnityEngine;
using System.Collections;



public class PlayerMovement : MonoBehaviour {

    [System.Serializable]
    public struct Point
    {
        [Range(0.05f,0.95f)]
        public float down, up;
    }

    public float movementSpeed;
    public string horizontalInputString;
    public string verticalInputString;

    Rigidbody2D rigid;

    public Point clampX;
    public Point clampY;

    public bool playerMoving
    {
        get
        {
            if (Input.GetAxis(horizontalInputString) == 0 && Input.GetAxis(verticalInputString) == 0)
            {
                return false;
            }
            return true;
        }

    }
    void Start () {
        rigid = GetComponent<Rigidbody2D>();
        
    }

  	void FixedUpdate () {
        if(Input.GetAxis(horizontalInputString) < 0)
        {
            rigid.AddForce(Vector3.left * movementSpeed,ForceMode2D.Impulse);
        }
        if (Input.GetAxis(horizontalInputString) > 0)
        {
            rigid.AddForce(Vector3.right* movementSpeed, ForceMode2D.Impulse);
        }
        if (Input.GetAxis(verticalInputString) < 0)
        {
            rigid.AddForce(Vector3.down* movementSpeed, ForceMode2D.Impulse);
        }
        if (Input.GetAxis(verticalInputString) > 0)
        {
            rigid.AddForce(Vector3.up * movementSpeed, ForceMode2D.Impulse);
        }
        clampMovement();
    }

    void clampMovement()
    {
        var pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp(pos.x, clampX.down,clampX.up);
        pos.y = Mathf.Clamp(pos.y, clampY.down,clampY.up);
        transform.position = Camera.main.ViewportToWorldPoint(pos);
    }
}
