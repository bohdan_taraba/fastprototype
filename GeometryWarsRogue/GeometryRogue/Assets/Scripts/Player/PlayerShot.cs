﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShot : MonoBehaviour {

    public string shotInputName;
    [HideInInspector]
    public List<WeaponHandler> weapons;

	void Start () {
        weapons = new List<WeaponHandler>(transform.GetComponentsInChildren<WeaponHandler>());
    }
	
	void Update () {
        if(Input.GetButton(shotInputName))
        {
            foreach(WeaponHandler w in weapons)
            {
                if(w.enabled)
                {
                    w.Shot();
                }
            }
        }
        
	}
}
