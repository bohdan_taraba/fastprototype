﻿using UnityEngine;
using System.Collections;

public class PointerHandler : MonoBehaviour {

    Vector3 mousePosition;

	void Start () {
	    
	}
	
	void Update () {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mousePosition.z = 0;
        transform.position = mousePosition;
	}

    public static Vector2 positionOfPointerToPlayer()
    {
        Vector2 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        Vector3 diff = mousePos - playerPos;
        diff.Normalize();
        return diff;
    }
}
