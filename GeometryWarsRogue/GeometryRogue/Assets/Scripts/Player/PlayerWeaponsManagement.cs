﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeaponsManagement : MonoBehaviour {

    public WeaponType activeWeapon;

    List<WeaponHandler> weapons;

    void Start() {
        

        Transform gun = transform.FindChild("gun");
        weapons = new List<WeaponHandler>(gun.GetComponentsInChildren<WeaponHandler>());

        foreach (WeaponHandler w in weapons)
        {
            if(w.type != activeWeapon)
            {
                w.enabled = false;
            }
        }
        

    }
    	
	void Update () {
		
	}

    public void switchWeapon(WeaponType typeToSwitch)
    {
        foreach (WeaponHandler w in weapons)
        {
            if (w.type != typeToSwitch)
            {
                w.enabled = false;
                continue;
            }
            w.enabled = true;
        }
    }

    public void switchRandomDifferentWeapon()
    {
        WeaponType newWeaponType = (WeaponType)Random.Range(1,
            System.Enum.GetValues(typeof(WeaponType)).Length);

        if(newWeaponType == activeWeapon)
        {
            switchRandomDifferentWeapon();
            return;
        }
        switchWeapon(newWeaponType);
    }
}
