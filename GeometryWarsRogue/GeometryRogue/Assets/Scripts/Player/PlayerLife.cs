﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerLife : MonoBehaviour {

    public List<string> tagsToDestroy;
    public int numberOfLives;
    public float fadeSpeed;
    public float respawnTime;


    public bool isPlayerAlive
    {
        get; private set;
    }

    //SpriteRenderer whiteboard;
    Color spriteColor;

    void Start () {
        //whiteboard = Camera.main.transform.FindChild("whiteboard").GetComponent<SpriteRenderer>();
        spriteColor = Color.white;
	}
	
	void Update () {
	}

    void OnTriggerEnter2D(Collider2D coll)
    {
        foreach(string s in tagsToDestroy)
        {
            if(coll.tag == s)
            {
                numberOfLives--;
                destroyAllEnemies();
                StartCoroutine(restartPlayer());
            }
        }
    }

    void destroyAllEnemies()
    {
        foreach(GameObject o in GameObject.FindGameObjectsWithTag("Enemy"))
        {
            o.GetComponent<EnemyLife>().destroyEnemy(false);
        }
    }

    IEnumerator restartPlayer()
    {
        switchPlayerComponents(false);
        yield return new WaitForSeconds(respawnTime);
        switchPlayerComponents(true);
        GetComponent<PlayerWeaponsManagement>().switchWeapon(WeaponType.Simple);
        transform.position = new Vector2(Camera.main.transform.position.x, Camera.main.transform.position.y);
        transform.localScale = Vector3.zero;
        StartCoroutine(resizePlayer());
    }

    IEnumerator resizePlayer()
    {
        while(transform.localScale.x < 1)
        {
            transform.localScale += new Vector3(0.1f,0.1f);
            yield return null;
        }
        
    }

    void switchPlayerComponents(bool value)
    {
        GetComponent<PlayerShot>().enabled = value;
        GetComponent<SpriteRenderer>().enabled = value;
        GetComponent<PlayerMovement>().enabled = value;
    }


}
