﻿using UnityEngine;
using System.Collections;

public class CameraLean : MonoBehaviour
{

    PlayerMovement plMovement;
    public float cameraRotationDelay = 0.5f;
    public float rotSpeed = 0.1f;
    public float maxRotationAngle = 5f;

    float mod;
    float delayTimer;
    float zVal = 0.0f;

    void Start()
    {
        mod = rotSpeed;
        delayTimer = cameraRotationDelay;
        plMovement = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerMovement>();
    }

    void Update()
    {
        cameraMove();
        
    }

    void cameraMove()
    {
        if (plMovement.playerMoving & delayTimer <= 0)
        {
            Vector3 rot = new Vector3(0, 0, zVal);
            transform.eulerAngles = rot;
            zVal += rotSpeed * Time.deltaTime;
            if (transform.localEulerAngles.z >= maxRotationAngle)
            {
                rotSpeed = -mod;
            }

            if (transform.eulerAngles.z >= 355f)
            {
                rotSpeed = +mod;
            }
        }
        else if (delayTimer > 0)
        {
            delayTimer -= Time.deltaTime;
        }
        else if (!plMovement.playerMoving)
        {
            delayTimer = cameraRotationDelay;
        }
    }
}
