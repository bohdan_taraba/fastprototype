﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AlpacaSound;

public class CameraChangeColors : MonoBehaviour {

    [System.Serializable]
    public struct Colors
    {
        public Color[] colorPool;
    }

    public Colors[] col;

    RetroPixel retroPixel;

    int actualColorPaleteIndex;

	void Start () {
        actualColorPaleteIndex = 0;
        retroPixel = GetComponent<RetroPixel>();

    }
	
	void Update () {
        /// test colors only
        if(Input.GetKeyDown(KeyCode.Space))
        {
            changeColors();
        }
	}

    public void changeColors()
    {
        int i = Random.Range(0,col.Length);
        if (i == actualColorPaleteIndex)
        {
            changeColors();
            return;
        }
            
        actualColorPaleteIndex = i;

        retroPixel.numColors = col[i].colorPool.Length;
        switch (col[i].colorPool.Length)
        {
            case 3:
                {
                    retroPixel.color0 = col[i].colorPool[0];
                    retroPixel.color1 = col[i].colorPool[1];
                    retroPixel.color2 = col[i].colorPool[2];
                    break;
                }
            case 4:
                {
                    retroPixel.color0 = col[i].colorPool[0];
                    retroPixel.color1 = col[i].colorPool[1];
                    retroPixel.color2 = col[i].colorPool[2];
                    retroPixel.color3 = col[i].colorPool[3];
                    break;
                }
            case 5:
                {
                    retroPixel.color0 = col[i].colorPool[0];
                    retroPixel.color1 = col[i].colorPool[1];
                    retroPixel.color2 = col[i].colorPool[2];
                    retroPixel.color3 = col[i].colorPool[3];
                    retroPixel.color4 = col[i].colorPool[4];
                    break;
                }
        }
    }

}
