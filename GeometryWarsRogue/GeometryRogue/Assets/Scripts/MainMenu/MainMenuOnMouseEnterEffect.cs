﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuOnMouseEnterEffect : MonoBehaviour {

    TextMesh text;
    float charSize;

	void Start () {
        text = GetComponent<TextMesh>();
        charSize = text.characterSize;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnMouseOver()
    {
        text.characterSize = 0.5f;
    }

    void OnMouseExit()
    {
        text.characterSize = charSize;
    }
}
