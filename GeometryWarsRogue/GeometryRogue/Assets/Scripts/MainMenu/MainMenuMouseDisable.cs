﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuMouseDisable : MonoBehaviour {

    public bool disableMouse;

    void Start () {
        Cursor.visible = false;
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
