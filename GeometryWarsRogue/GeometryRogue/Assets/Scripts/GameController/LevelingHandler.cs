﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelingHandler : MonoBehaviour {

    public int xpForNextLevel;

    [Range(0f,1f)]
    public float percentageIncreaseForLevel;

    static int experiences;
    public static int actualLevel;

    CameraChangeColors colors;

    PlayerWeaponsManagement weapon;

    void Start () {
        colors = Camera.main.GetComponent<CameraChangeColors>();
        actualLevel = 1;
        weapon = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerWeaponsManagement>();
	}
	
	// Update is called once per frame
	void Update () {
        if(experiences > xpForNextLevel)
        {
            dingNextLevel();
        }

	}

    public void addExperiences(int exp)
    {
        experiences += exp;
    }

    void dingNextLevel()
    {
        experiences = experiences - xpForNextLevel;
        actualLevel++;
        xpForNextLevel = xpForNextLevel + (int)(xpForNextLevel * percentageIncreaseForLevel);
        colors.changeColors();

        weapon.switchRandomDifferentWeapon();

    }
}
