﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyExplosionChunk : MonoBehaviour {

    public float timeToLive;
    public float expansionSpeed;

	void Start () {
        transform.localScale = Vector2.zero;
	}
	
	void Update () {
        transform.localScale += new Vector3(expansionSpeed,expansionSpeed) * Time.deltaTime;

        timeToLive -= Time.deltaTime;
        if(timeToLive <= 0)
        {
            Destroy(gameObject);
        }
	}
}
