﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyLife : MonoBehaviour {

    public float startLives;

    public int xpOnDeath;

    [Range(0f, 0.01f)]
    public float shakeModificator;

    [Tooltip("tato hodnota by mela zastupovat velikost a objem nepritele")]
    public float shakeBase;

    public float explosionLenght;

    public GameObject explosionChunk;
    public int numberOfChunks;
    public float chunksSpan;

    float shakeAmt = 0;

    Camera mainCamera;

    Vector3 startCamPosition;
    SpriteRenderer sr;
    Collider2D enemyColl;

    public float actualLives
    {
        get; private set;
    }


    void Start () {
        sr = GetComponent<SpriteRenderer>();
        enemyColl = GetComponent<Collider2D>();

        mainCamera = Camera.main;
        actualLives = startLives;
        startCamPosition = mainCamera.transform.position;
	}

    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Shot")
        {
            ShotBase shot = coll.gameObject.GetComponent<ShotBase>();
            actualLives -= shot.shotDamage;
            shot.destroyShot();

            if (actualLives <= 0)
            {
                
                destroyEnemy(true);
            }
        }
    }

    public void destroyEnemy(bool destroyedByShot)
    {
        if(destroyedByShot)
            GameObject.FindGameObjectWithTag("GameController").GetComponent<LevelingHandler>().addExperiences(xpOnDeath);
        shakeAmt = shakeBase * shakeModificator;
        disableBeforeDestroy();
        initChunks();
        InvokeRepeating("CameraShake", 0, .01f);
        Invoke("StopShaking", explosionLenght);
    }


    void disableBeforeDestroy()
    {
        enemyColl.enabled = false;
        sr.enabled = false;
        foreach(SpriteRenderer r in transform.GetComponentsInChildren<SpriteRenderer>())
        {
            r.enabled = false;
        }
        foreach (Collider2D c in transform.GetComponentsInChildren<Collider2D>())
        {
            c.enabled = false;
        }
    }

    void CameraShake()
    {
        if (shakeAmt > 0f)
        {
            Vector3 pp = mainCamera.transform.position;
            pp.y += getRandomValue();
            pp.x += getRandomValue();
            mainCamera.transform.position = pp;
        }
    }

    void StopShaking()
    {
        mainCamera.transform.position = startCamPosition;
        CancelInvoke("CameraShake");
        Destroy(gameObject);
    }

    float getRandomValue()
    {
        return Random.value * shakeAmt * 2 - shakeAmt;
    }

    void initChunks()
    {
        int chunksAmount = 0;
        StartCoroutine(createChunk(chunksAmount));
    }

    IEnumerator createChunk(int chunksAmount)
    {
        if(chunksAmount > numberOfChunks)
        {
            yield break;
        }
        yield return new WaitForSeconds(chunksSpan);
        GameObject o = Instantiate(explosionChunk);
        o.transform.position = randomChunkSpawnPos();
        chunksAmount++;
        StartCoroutine(createChunk(chunksAmount));

    }

    Vector3 randomChunkSpawnPos()
    {
        float width = sr.bounds.size.x/2;
        float height = sr.bounds.size.y/2;

        return new Vector3(transform.position.x + Random.Range(-width,width),
            transform.position.y + Random.Range(-height, height));

    }
}
