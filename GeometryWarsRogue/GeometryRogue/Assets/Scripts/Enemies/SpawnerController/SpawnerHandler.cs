﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerHandler : MonoBehaviour {

    public List<GameObject> enemiesToSpawn;
    public float spawnSpan;


    List<Transform> spawnPoints;

	void Start () {
        spawnPoints = new List<Transform>();
        foreach (Transform t in transform)
            spawnPoints.Add(t);

        foreach(Transform t in spawnPoints)
        {
            StartCoroutine(spawnEnemy(t.position));
        }
        //print(enemiesToSpawn.Count);

    }

    IEnumerator spawnEnemy(Vector3 pos)
    {
        yield return new WaitForSeconds(spawnSpan);
        Instantiate(enemiesToSpawn[Random.Range(0,enemiesToSpawn.Count)],pos,Quaternion.identity);
        StartCoroutine(spawnEnemy(pos));
    }

	// Update is called once per frame
	void Update () {
		
	}
}
